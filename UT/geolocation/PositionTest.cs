﻿using FluentAssertions;
using System.Diagnostics.CodeAnalysis;


namespace UT.geolocation
{
    public class PositionTest
    {
        [Fact]
        public void assertEquality()
        {
            Position a = new Position(5, 2);
            Position b = new Position(5, 2);

            a.Should().Be(b);
        }

        [Fact]
        public void assertInequality()
        {
            Position a = new Position(5, 2);
            Position b = new Position(7, 4);

            a.Should().NotBe(b);
        }

        [Fact]
        public void should_find_position_at_north()
        {

            Orientation.North.of(@"
                            ┌─────┐
                            │     │
                            │  X  │
                            └─────┘"
            ).ShouldBe(@"
                            ┌─────┐
                            │  X  │
                            │     │
                            └─────┘");
        }

        [Fact]
        public void should_find_position_at_south()
        {

            Orientation.South.of(@"
                            ┌─────┐
                            │  X  │
                            │     │
                            └─────┘"
            ).ShouldBe(@"
                            ┌─────┐
                            │     │
                            │  X  │
                            └─────┘");
        }

        [Fact]
        public void should_find_position_at_east()
        {

            Orientation.East.of(@"
                            ┌───┐
                            │ X │
                            └───┘"
            ).ShouldBe(@"
                            ┌───┐
                            │  X│
                            └───┘");
        }

        [Fact]
        public void should_find_position_at_west()
        {

            Orientation.West.of(@"
                            ┌───┐
                            │ X │
                            └───┘"
            ).ShouldBe(@"
                            ┌───┐
                            │X  │
                            └───┘");
        }

        public Position At(string description)
        {
            return description.Find();
        }
    }


    public static class Ext
    {
        public static Position of(this Orientation orientation, string description)
        {
            var position = description.Find();

            return position.Project(orientation, 1.meters());
        }

        public static Position Find(this string description)
        {
            var lines = description.Split("\n").Select(x => x.Trim()).Reverse();
            var Y = lines.TakeWhile(x => !x.Contains('X')).Count();
            var X = lines.Where(x => x.Contains('X')).First().IndexOf("X");
            return new Position(X, Y);
        }

        public static void ShouldBe(this Position actual, string description)
        {
            var expected = description.Find();

            actual.Should().Be(expected);
        }
    }
}
﻿using FluentAssertions;

namespace UT.geolocation
{
    public class OrientationTest
    {
        [Fact]
        void should_turn_left()
        {
            Orientation.North
                .when_turning_left_should_be(Orientation.West)
                .when_turning_left_should_be(Orientation.South)
                .when_turning_left_should_be(Orientation.East)
                .when_turning_left_should_be(Orientation.North);
        }

        [Fact]
        void should_turn_right()
        {
            Orientation.North
                .when_turning_right_should_be(Orientation.East)
                .when_turning_right_should_be(Orientation.South)
                .when_turning_right_should_be(Orientation.West)
                .when_turning_right_should_be(Orientation.North);
        }
    }
}


static class OrientationMethods
{
    public static Orientation when_turning_left_should_be(this Orientation actual, Orientation expected)
    {
        actual.Left().Should().Be(expected);
        return expected;
    }

    public static Orientation when_turning_right_should_be(this Orientation actual, Orientation expected)
    {
        actual.Right().Should().Be(expected);
        return expected;
    }
}
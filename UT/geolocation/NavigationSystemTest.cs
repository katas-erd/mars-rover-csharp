﻿using FluentAssertions;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Metrics;
using System.Drawing;
using System.Numerics;


namespace UT.geolocation
{
    public class NavigationSystemTest
    {
        [Fact]
        public void LandAtTheGivenPosition()
        {
            GivenAPlanetOfSize(4, 5)
            .WhenLandingAt(new Position(2, 3))
            .ThenItShouldBeAt(new Position(2, 3));
        }

        [Fact]
        public void GoToTheGivenPosition()
        {
            GivenAPlanetOfSize(10, 10)
                .WhenLandingAt(new Position(2, 1))
                .WhenGoingTo(Orientation.North, 2.meters())
                .ThenItShouldBeAt(new Position(2, 3));
        }

        [Fact]
        public void BoundToTheEdges()
        {
            GivenAPlanetOfSize(10, 10)
                .WhenLandingAt(new Position(2, 9))
                .WhenGoingTo(Orientation.North, 1.meters())
                .ThenItShouldBeAt(new Position(2, 0));
        }

        [Fact]
        public void DetectObstacles()
        {
            WhenLandingOnThePlanet(@"
                        ┌─────┐
                        │ #   │
                        │ X   │
                        │     │
                        └─────┘")
                .ThenTheRoverCannotGo(Orientation.North)
                .ThenTheRoverCanGo(Orientation.East)
                .ThenTheRoverCanGo(Orientation.West)
                .ThenTheRoverCanGo(Orientation.South)
                ;
        }



        private Size GivenAPlanetOfSize(int width, int height)
        {
            return new Size { Width = width, Height = height };
        }

        /*
    [Fact]
    fun `detect obstacles`() {
            `when landing on the planet`("""
                            ┌─────┐
                            │ #   │
                            │ X   │
                            │     │
                            └─────┘
                            """
            ).`then the rover cannot go`(Compas.North)
            .`then the rover can go`(Compas.South)
            .`then the rover can go`(Compas.East)
            .`then the rover can go`(Compas.West)
        }*/


        private NavigationSystem WhenLandingOnThePlanet(string description)
        {
            return description.SingleGrid().coordinateSystem();
        }

    }
    public static class NSExt
    {
        public static NavigationSystem WhenLandingAt(this Size size, Position position)
        {
            return new NavigationSystem(position, new Planet(size, new List<Obstacle>(), new List<Domain.geolocation.Location>()));
        }


        public static NavigationSystem ThenTheRoverCannotGo(this NavigationSystem ns, Orientation orientation)
        {
            ns.GoTo(orientation, 1.meters()).Should().Be(RoverStatus.WONTCO);
            return ns;
        }

        public static NavigationSystem ThenTheRoverCanGo(this NavigationSystem ns, Orientation orientation)
        {
            ns.GoTo(orientation, 1.meters()).Should().Be(RoverStatus.HAVCO);
            return ns;
        }

        public static NavigationSystem WhenGoingTo(this NavigationSystem ns, Orientation orientation, Distance distance)
        {
            ns.GoTo(orientation, distance);
            return ns;
        }

        public static NavigationSystem ThenItShouldBeAt(this NavigationSystem ns, Position expected)
        {
            ns.Position().Should().Be(expected);
            return ns;
        }

    }
}
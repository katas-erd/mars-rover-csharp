﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UT.geolocation;

namespace UT.mission
{
    public class RoverShould
    {
        [Fact]
        void MoveForward()
        {
            Calling((Rover rover) => rover.moveForward()).ShouldMoveTheRoverInTheRightDirection(
                @"
                ┌─────┐    ┌─────┐
                │     │    │ ^   │
                │ ^   │    │     │
                └─────┘    └─────┘
                ",
                @"
                ┌─────┐    ┌─────┐
                │ v   │    │     │
                │     │    │ v   │
                └─────┘    └─────┘
                ",
                @"
                ┌─────┐    ┌─────┐
                │     │    │     │
                │>    │    │ >   │
                └─────┘    └─────┘
                ",
                @"
                ┌─────┐    ┌─────┐
                │     │    │     │
                │    <│    │   < │
                └─────┘    └─────┘
                "
            );
        }

        [Fact]
        void MoveBackward()
        {
            Calling((Rover rover) => rover.moveBackward()).ShouldMoveTheRoverInTheRightDirection(
                    @"
                    ┌─────┐    ┌─────┐
                    │ ^   │    │     │
                    │     │    │ ^   │
                    └─────┘    └─────┘
                    ",
                    @"
                    ┌─────┐    ┌─────┐
                    │     │    │ v   │
                    │ v   │    │     │
                    └─────┘    └─────┘
                    ",
                    @"
                    ┌─────┐    ┌─────┐
                    │     │    │     │
                    │ >   │    │>    │
                    └─────┘    └─────┘
                    ",
                    @"
                    ┌─────┐    ┌─────┐
                    │     │    │     │
                    │   < │    │    <│
                    └─────┘    └─────┘
                    "
            );
        }

        [Fact]
        void TurnRight()
        {
            Calling((Rover rover) => rover.turnRight()).ShouldMoveTheRoverInTheRightDirection(
                    @"
                    ┌─────┐    ┌─────┐
                    │ ^   │    │ >   │
                    │     │    │     │
                    └─────┘    └─────┘
                    ",
                    @"
                    ┌─────┐    ┌─────┐
                    │ >   │    │ v   │
                    │     │    │     │
                    └─────┘    └─────┘
                    ",
                    @"
                    ┌─────┐    ┌─────┐
                    │ v   │    │ <   │
                    │     │    │     │
                    └─────┘    └─────┘
                    ",
                    @"
                    ┌─────┐    ┌─────┐
                    │ <   │    │ ^   │
                    │     │    │     │
                    └─────┘    └─────┘
                    "
            );
        }


        [Fact]
        void TurnLeft()
        {
            Calling((Rover rover) => rover.turnLeft()).ShouldMoveTheRoverInTheRightDirection(
                    @"
                    ┌─────┐    ┌─────┐
                    │ ^   │    │ <   │
                    │     │    │     │
                    └─────┘    └─────┘
                    ",
                    @"
                    ┌─────┐    ┌─────┐
                    │ >   │    │ ^   │
                    │     │    │     │
                    └─────┘    └─────┘
                    ",
                    @"
                    ┌─────┐    ┌─────┐
                    │ v   │    │ >   │
                    │     │    │     │
                    └─────┘    └─────┘
                    ",
                    @"
                    ┌─────┐    ┌─────┐
                    │ <   │    │ v   │
                    │     │    │     │
                    └─────┘    └─────┘
                    "
            );
        }

        [Fact]
        void MoveAroundThePlanet()
        {
            Calling((Rover rover) => rover.moveForward()).ShouldMoveTheRoverInTheRightDirection(
                    @"
                    ┌─────┐    ┌─────┐
                    │  ^  │    │     │
                    │     │    │     │
                    │     │    │     │
                    │     │    │  ^  │
                    └─────┘    └─────┘
                    ",
                    @"
                    ┌─────┐    ┌─────┐
                    │     │    │  v  │
                    │     │    │     │
                    │     │    │     │
                    │  v  │    │     │
                    └─────┘    └─────┘
                    ",
                    @"
                    ┌───────┐    ┌───────┐
                    │      >│    │>      │
                    │       │    │       │
                    └───────┘    └───────┘
                    ",
                    @"
                    ┌───────┐    ┌───────┐
                    │<      │    │      <│
                    │       │    │       │
                    └───────┘    └───────┘
                    "
            );
        }

        [Fact]
        void StopInFrontOfAnObstacle()
        {
            Calling((Rover rover) => rover.moveForward()).ShouldMoveTheRoverInTheRightDirection(
                @"
                ┌─────┐ ┌─────┐ ┌─────┐
                │  v  │ │     │ │     │
                │     │ │  v  │ │  v  │
                │  #  │ │  #  │ │  #  │
                │     │ │     │ │     │
                └─────┘ └─────┘ └─────┘
                ",
                @"
                ┌───────┐ ┌───────┐ ┌───────┐
                │> #    │ │ >#    │ │ >#    │
                │       │ │       │ │       │
                └───────┘ └───────┘ └───────┘
                ",
                @"
                ┌───────┐ ┌───────┐ ┌───────┐
                │  # <  │ │  #<   │ │  #<   │
                │       │ │       │ │       │
                └───────┘ └───────┘ └───────┘
                ",
                @"
                ┌─────┐ ┌─────┐ ┌─────┐
                │  #  │ │  #  │ │  #  │
                │     │ │  ^  │ │  ^  │
                │  ^  │ │     │ │     │
                │     │ │     │ │     │
                └─────┘ └─────┘ └─────┘
                "
            );
        }

        [Fact]
        void StopInFrontOfAnObstacleAroundThePLanet()
        {
            Calling((Rover rover) => rover.moveForward()).ShouldMoveTheRoverInTheRightDirection(
            @"
        ┌─────┐ ┌─────┐ ┌─────┐
        │  #  │ │  #  │ │  #  │
        │     │ │     │ │     │
        │  v  │ │     │ │     │
        │     │ │  v  │ │  v  │
        └─────┘ └─────┘ └─────┘
        ",
        @"
        ┌───────┐ ┌───────┐ ┌───────┐
        │#    > │ │#     >│ │#     >│
        │       │ │       │ │       │
        └───────┘ └───────┘ └───────┘
        ",
        @"
        ┌───────┐ ┌───────┐ ┌───────┐
        │ <    #│ │<     #│ │<     #│
        │       │ │       │ │       │
        └───────┘ └───────┘ └───────┘
        ",
        @"
        ┌─────┐ ┌─────┐ ┌─────┐
        │     │ │  ^  │ │  ^  │
        │  ^  │ │     │ │     │
        │     │ │     │ │     │
        │  #  │ │  #  │ │  #  │
        └─────┘ └─────┘ └─────┘
        "
            );
        }



        TestHelper Calling(Action<Rover> f)
        {
            return new TestHelper(f);
        }
    }


    class TestHelper
    {
        private Action<Rover> f;

        public TestHelper(Action<Rover> f)
        {
            this.f = f;
        }

        public void ShouldMoveTheRoverInTheRightDirection(params String[] usecases)
        {
            for (int i = 0; i < usecases.Length; i++)
            {
                String usecase = usecases[i];
                var grids = usecase.Grids();
                var previous = grids.First().rover();
                grids.RemoveAt(0);
                grids.ForEach((grid) =>
                {
                    f(previous);
                    var expected = grid.rover();

                    previous.Position().Should().Be(expected.Position());
                    previous.Orientation().Should().Be(expected.Orientation());
                });
            }
        }
    }
}
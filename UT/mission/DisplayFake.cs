﻿using Domain.mission;

namespace UT.mission
{
    internal class DisplayFake : MissionControlMonitor
    {
        Report report;
        public void display(Report report)
        {
            this.report = report;
        }
    }

}

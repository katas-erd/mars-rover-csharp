﻿using Domain.mission;
using FluentAssertions;
using System;

namespace UT.mission
{
    public class CommandShould
    {
        [Fact]
        void moveForward()
        {
            GivenTheFollowingSituation(
                @"
                ┌─────┐
                │     │
                │ ^   │
                └─────┘ 
                "
            ).WhenWeSendTheCommand("f")
            .ThenWeShouldSee(@"
                ┌─────┐
                │ ^   │
                │     │
                └─────┘ 
            ");
        }

        [Fact]
        void moveBackward()
        {
            GivenTheFollowingSituation(
                @"
                ┌─────┐
                │ ^   │
                │     │
                │     │
                └─────┘ 
                "
            ).WhenWeSendTheCommand("b")
            .ThenWeShouldSee(@"
                ┌─────┐
                │     │
                │ ^   │
                │     │
                └─────┘ 
            ");
        }

        [Fact]
        void TurnLeft()
        {
            GivenTheFollowingSituation(
                @"
                ┌─────┐
                │ ^   │
                │     │
                └─────┘ 
                "
            ).WhenWeSendTheCommand("l")
            .ThenWeShouldSee(@"
                ┌─────┐
                │ <   │
                │     │
                └─────┘ 
            ");
        }

        [Fact]
        void TurnRight()
        {
            GivenTheFollowingSituation(
                @"
                ┌─────┐
                │ ^   │
                │     │
                └─────┘ 
                "
            ).WhenWeSendTheCommand("r")
            .ThenWeShouldSee(@"
                ┌─────┐
                │ >   │
                │     │
                └─────┘ 
            ");
        }

        [Fact]
        void ChainCommands()
        {
            GivenTheFollowingSituation(
                @"
        ┌─────┐
        │ ^   │
        │     │
        └─────┘ 
        ").WhenWeSendTheCommand("rffrflb")
                .ThenWeShouldSee(
                    @"
        ┌─────┐ ┌─────┐ ┌─────┐ ┌─────┐ ┌─────┐ ┌─────┐ ┌─────┐ 
        │>    │ │  >  │ │   > │ │   v │ │     │ │     │ │     │ 
        │     │ │     │ │     │ │     │ │   v │ │   > │ │  >  │ 
        └─────┘ └─────┘ └─────┘ └─────┘ └─────┘ └─────┘ └─────┘  
        ");
        }

        [Fact]
        void RejectWrongCommand()
        {
            GivenTheFollowingSituation(
                @"
            ┌─────┐
            │ ^   │
            │     │
            └─────┘ 
            "
            ).WhenWeSendTheCommand("Z")
            .ThenWeShouldGetAnError();
        }

        private CommandTestHelper GivenTheFollowingSituation(string v)
        {
            return new CommandTestHelper(v.SingleGrid());
        }
    }
    class CommandTestHelper
    {
        private HelperExtMethods.Grid grid;
        private Rover rover;
        private ControlPanel controlPanel;
        private RoverStatus status;

        public CommandTestHelper(HelperExtMethods.Grid grid)
        {
            this.grid = grid;
            rover = grid.rover();
            controlPanel = ControlPanel.Piloting(rover);
        }

        internal void ThenWeShouldGetAnError()
        {
            status.Should().NotBe(RoverStatus.HAVCO);
        }

        internal void ThenWeShouldSee(string expected)
        {
            var expectedRover = expected.Grids().Last().rover();

            this.status.Should().Be(RoverStatus.HAVCO);
            this.rover.Position().Should().Be(expectedRover.Position());
            this.rover.Orientation().Should().Be(expectedRover.Orientation());

        }

        internal CommandTestHelper WhenWeSendTheCommand(string commands)
        {
            status = controlPanel.Send(commands);
            return this;
        }
    }
}

﻿using FluentAssertions;
using System.Text.RegularExpressions;
using Domain.geolocation;

namespace UT
{
    static class HelperExtMethods
    {
        public static Orientation toOrientation(this String s)
        {
            switch (s)
            {
                case "^":
                    return Orientation.North;
                case "v":
                    return Orientation.South;
                case "<":
                    return Orientation.West;
                case ">":
                    return Orientation.East;
                default:
                    return Orientation.North;
            }
        }

        public static Grid SingleGrid(this String content)
        {
            return content.Unindent().Split("\n").Select(s => s.Trim()).Reverse().ToList().SingleGridFromLines();
        }

        public static List<Grid> Grids(this String content)
        {
            var lines = content.Unindent().Split("\n");
            var columnFirstActual = lines.First().IndexOf('┌');
            var grids = new List<Grid>();

            while (columnFirstActual != -1) {
                var columnLastActual = lines.First().IndexOf('┐', columnFirstActual);
                List<String> gridLines = lines.Select(line => line.Substring(columnFirstActual, columnLastActual - columnFirstActual + 1)).Reverse().ToList();
                grids.Add(
                    gridLines.SingleGridFromLines()
                ) ;
                columnFirstActual = lines.First().IndexOf('┌', columnFirstActual + 1);
            }

            return grids;

        }

        private static Grid SingleGridFromLines(this List<String> lines)
        {
            var cases = lines.Where((i, idx) => idx > 0 && idx < lines.Count - 1).Select(s =>
            {
                var start = s.IndexOf("│") + 1;
                var end = s.IndexOf("│", start + 1);
                return s.Substring(start, end - start);
            }
            ).ToList();
            
            var width = cases.First().Length;
            var height = cases.Count;

            var all = cases.SelectMany((s, y) => s.Select((c, x) => new Item { Position = new Position(x, y), Symbol = c.ToString() })).ToList();
            var items = all.Where(i => i.Symbol != " ").ToList();

            return new Grid { size = new Size { Width = width, Height = height }, items = items, legends = lines.legends()};
        }

        public static List<Legend> legends(this List<String> lines)
        {
            return lines.SelectMany(s =>
            {
                var rx = new Regex(@".*(\d)\. (.+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                if (rx.IsMatch(s))
                {
                    var group = rx.Match(s).Groups;
                    return new List<Legend> { new Legend { number = Int32.Parse(group[1].Value), text = group[2].Value } };
                }
                else
                {
                    return new List<Legend>();
                }
            }).ToList();
        }

        public static string Unindent(this string source)
        {
            IEnumerable<string> lines = source.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            lines = TrimEmptyLines(lines);
            string indent = new string((lines.FirstOrDefault() ?? "").TakeWhile(char.IsWhiteSpace).ToArray());
            lines = lines.Select(l => l.StartsWith(indent) ? l.Substring(indent.Length) : l.TrimStart(' '));
            return string.Join(Environment.NewLine, lines.ToArray());
        }

        private static IEnumerable<string> TrimEmptyLines(IEnumerable<string> source)
        {
            string[] sourceArr = source.ToArray();

            // Remove empty lines one by one from beginning and end
            int start = 0, end = sourceArr.Length - 1;
            while (start < end && sourceArr[start].All(char.IsWhiteSpace)) start++;
            while (end >= start && sourceArr[end].All(char.IsWhiteSpace)) end--;
            return sourceArr.Skip(start).Take(end - start + 1);
        }

        public record Item
        {
            public string Symbol { get; init; }
            public Position Position { get; init; }
        }

        public record Legend
        {
            public string text { get; init; }
            public int number { get; init; }
        }

        public record Grid
        {
            public Size size { get; init; }
            public List<Item> items { get; init; }
            public List<Legend> legends { get; init; }

            public List<Obstacle> obstacles()
            {
                return items.Where(x => x.Symbol == "#").Select(x => new Obstacle(x.Position)).ToList();
            }

            public Rover rover()
            {
                var rover = items.Where(it => "^<>v".Contains(it.Symbol)).First();
                return new Rover(new NavigationSystem(rover.Position, new Planet(size, this.obstacles(), this.locations())), rover.Symbol.toOrientation());
            }

            public Position Position()
            {
                return items.Where(it => "X^<>v".Contains(it.Symbol)).First().Position;
            }

            public NavigationSystem coordinateSystem()
            {
                return new NavigationSystem(this.Position(), this.Planet());
            }

            public Planet Planet()
            {
                return new Planet(this.size, this.obstacles(), this.locations());
            }

            public List<Location> locations()
            {
                return legends.Select(point => new Location(point.text, items.Where(it => it.Symbol == point.number.ToString()).First().Position)).ToList();
            }
        }
    }
}

﻿using Domain.geolocation;
using FluentAssertions;
using System.Text.RegularExpressions;

namespace UT
{
    public class TestHelperTest
    {
        [Fact]
        void ShouldCreateAGrid()
        {
            var grid = @"
                        ┌──┐
                        │  │
                        │# │
                        │^.│
                        └──┘ 
                        ".SingleGrid();
            grid.items.Should().BeEquivalentTo(new List<HelperExtMethods.Item> {
                    new HelperExtMethods.Item{ Symbol = "^", Position = new Position(0, 0) },
                    new HelperExtMethods.Item{ Symbol ="#", Position = new Position(0, 1) },
                    new HelperExtMethods.Item{ Symbol = ".", Position = new Position(1, 0) }
            });
        }

        [Fact]
        void ShouldCreateGrids()
        {
            var grid = @"
                        ┌──┐ ┌──┐
                        │# │ │ ;│
                        │^.│ │  │
                        └──┘ └──┘ 
                        ".Grids();

            grid.Count().Should().Be(2);
            grid.First().items.Should().BeEquivalentTo(new List<HelperExtMethods.Item> {
                    new HelperExtMethods.Item{ Symbol = "^", Position = new Position(0, 0) },
                    new HelperExtMethods.Item{ Symbol ="#", Position = new Position(0, 1) },
                    new HelperExtMethods.Item{ Symbol = ".", Position = new Position(1, 0) }
            });
            grid.Last().items.Should().BeEquivalentTo(new List<HelperExtMethods.Item> {
                    new HelperExtMethods.Item{ Symbol = ";", Position = new Position(1, 1) },
            });
        }

        [Fact]
        void ShouldFindOrientation()
        {
            var grid = @"
                        ┌──┐ ┌──┐ ┌──┐ ┌──┐
                        │  │ │  │ │  │ │  │
                        │> │ │^ │ │< │ │v │
                        │  │ │  │ │  │ │  │
                        └──┘ └──┘ └──┘ └──┘  
                        ".Grids();

            grid.Count().Should().Be(4);
            grid.Select(grid => grid.rover().Orientation()).Should().ContainInConsecutiveOrder(
                Orientation.East,
                Orientation.North,
                Orientation.West,
                Orientation.South
            );
        }

        [Fact]
        void ShouldFindObstacles()
        {
            var grid = @"
                        ┌──┐ ┌──┐ 
                        │  │ │# │ 
                        │##│ │  │ 
                        │  │ │# │ 
                        └──┘ └──┘ 
                        ".Grids();

            grid.Count().Should().Be(2);
            grid.First().obstacles().Should().ContainInConsecutiveOrder(
                new Obstacle(new Position(0, 1)), new Obstacle(new Position(1, 1)));

            grid.Last().obstacles().Should().ContainInConsecutiveOrder(
                new Obstacle(new Position(0, 0)), new Obstacle(new Position(0, 2)));
        }

        [Fact]
        void ShouldFindLocations()
        {
            var grid = @"
                        ┌──┐
                        │  │
                        │1 │
                        │  │ 1. Point A
                        └──┘ 
                        ".SingleGrid();

            grid.locations().Should().Contain(
                new Location("Point A", new Position(0, 1))
            ); ;
        }
    }
}

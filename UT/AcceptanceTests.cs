﻿using Domain;
using Domain.mission;
using FluentAssertions;
using UT.mission;

namespace UT
{
    public class AcceptanceTests
    {


        Planet Mars = @"
                ┌─────────────────────────────┐
                │                       #     │
                │                       #     │
                │   #    #     1              │
                │                   #         │
                │                             │
                │    ##    ##  3          #   │
                │    ##             #         │  1. Jezero
                │                             │  2. Aeolis Pallus
                │           #       #    #   2│  3. Meridian Plains
                └─────────────────────────────┘".SingleGrid().Planet();

        MissionControlMonitor displayFake = new DisplayFake();



        [Fact(DisplayName = "You are given the initial starting point(x, y) of a rover and the direction(N, S, E, W) it is facing")]
        void Test1()
        {
            var mission = MissionInitiation.Land(
                the_rover: "Curiosity",
                on_the_planet: Mars,
                at_site: "Jezero",
                facing: Orientation.South,
                transmitting_report_to: displayFake
            );

            var report = mission.Transmit("");
            report.Position.Should().Be(Mars.LocationOf("Jezero"));
            report.Orientation.Should().Be(Orientation.South);
            report.Status.Should().Be(RoverStatus.HAVCO);
        }

        [Fact(DisplayName = "Implement commands that move the rover forward (f)")]
        void test2()
        {
            var mission = MissionInitiation.Land(
            the_rover: "Curiosity",
            on_the_planet: Mars,
            at_site: "Jezero",
            facing: Orientation.South,
            transmitting_report_to: displayFake
        );

            var report = mission.Transmit("bb");

            report.Position.Should().Be(Mars.LocationOf("Jezero").Project(Orientation.North, 2.meters()));
            report.Orientation.Should().Be(Orientation.South);
            report.Status.Should().Be(RoverStatus.HAVCO);
        }

        [Fact(DisplayName = "Implement commands that move the rover backward (b)")]
        void test3()
        {
            var mission = MissionInitiation.Land(
            the_rover: "Curiosity",
            on_the_planet: Mars,
            at_site: "Jezero",
            facing: Orientation.South,
            transmitting_report_to: displayFake
        );

            var report = mission.Transmit("ff");

            report.Position.Should().Be(Mars.LocationOf("Jezero").Project(Orientation.South, 2.meters()));
            report.Orientation.Should().Be(Orientation.South);
            report.Status.Should().Be(RoverStatus.HAVCO);
        }

        [Fact(DisplayName = "Implement commands that turn the rover left (l)")]
        void test4()
        {
            var mission = MissionInitiation.Land(
            the_rover: "Curiosity",
            on_the_planet: Mars,
            at_site: "Jezero",
            facing: Orientation.South,
            transmitting_report_to: displayFake
        );

            var report = mission.Transmit("l");

            report.Position.Should().Be(Mars.LocationOf("Jezero"));
            report.Orientation.Should().Be(Orientation.East);
            report.Status.Should().Be(RoverStatus.HAVCO);
        }

        [Fact(DisplayName = "Implement commands that turn the rover right (r)")]
        void test5()
        {
            var mission = MissionInitiation.Land(
            the_rover: "Curiosity",
            on_the_planet: Mars,
            at_site: "Jezero",
            facing: Orientation.South,
            transmitting_report_to: displayFake
        );

            var report = mission.Transmit("r");

            report.Position.Should().Be(Mars.LocationOf("Jezero"));
            report.Orientation.Should().Be(Orientation.West);
            report.Status.Should().Be(RoverStatus.HAVCO);
        }

        [Fact(DisplayName = "Implement wrapping at edges But be careful, planets are spheres")]
        void test6()
        {
            var mission = MissionInitiation.Land(
            the_rover: "Curiosity",
            on_the_planet: Mars,
            at_site: "Aeolis Pallus",
            facing: Orientation.East,
            transmitting_report_to: displayFake
        );

            var report = mission.Transmit("f");

            report.Position.Should().Be(new Position(0, 0));
            report.Orientation.Should().Be(Orientation.East);
            report.Status.Should().Be(RoverStatus.HAVCO);
        }

        [Fact(DisplayName = "Implement obstacle detection before each move to a new square")]
        void test7()
        {
            var mission = MissionInitiation.Land(
            the_rover: "Curiosity",
            on_the_planet: Mars,
            at_site: "Meridian Plains",
            facing: Orientation.West,
            transmitting_report_to: displayFake
        );

            var report = mission.Transmit("ffff");

            report.Position.Should().Be(Mars.LocationOf("Meridian Plains").Project(Orientation.West, 2.meters()));
            report.Orientation.Should().Be(Orientation.West);
            report.Status.Should().Be(RoverStatus.WONTCO);
        }
    }
}



/*
@Test



    @Test
    fun ``() {
        var mission = MissionInitiation.Land(
            the_rover:"Curiosity",
            on_the_planet:Mars,
            at_site:"Meridian Plains".location,
            facing:Compas.West,
            transmitting_report_to:displayFake
        )

        var report = mission.transmit("ffff")

        assertThat(report.position).isEqualTo("Meridian Plains".location.west(2.meters))
        assertThat(report.orientation).isEqualTo(Compas.West)
        assertThat(report.status).isEqualTo(RoverStatus.WONTCO)
    }

*/
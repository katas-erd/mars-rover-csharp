﻿using Domain.mission;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{

    public static class MissionInitiation {
        public static Mission Land(string the_rover, Planet on_the_planet, string at_site, Orientation facing, MissionControlMonitor transmitting_report_to)
        {
            var position = on_the_planet.LocationOf(at_site);
            var navigationSystem = new NavigationSystem(position, on_the_planet);
            var rover = new Rover(
                            guidedWith: navigationSystem,
                            facing: facing
                        );
            var control = ControlPanel.Piloting(rover);

            return new Mission(
                named: the_rover,
                controledBy: control,
                sendingReportTo: transmitting_report_to
            );
        }
    }
}

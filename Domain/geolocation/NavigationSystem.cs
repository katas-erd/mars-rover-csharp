﻿public class NavigationSystem {
    private readonly Planet planet;
    private Position currentPosition;

    public NavigationSystem(Position position, Planet planet)
    {
        this.currentPosition = position;
        this.planet = planet;
    }

    public Position Position()
    {
        return currentPosition;
    }
    
    
    public RoverStatus GoTo(Orientation orientation, Distance distance) {
        var nextPosition = planet.RealPositionFrom(
            Position().Project(orientation, distance)
        );

        if (HasSomethingBlockingTheWayAt(nextPosition)) {
            return RoverStatus.WONTCO;
        }
        currentPosition = nextPosition;
        return RoverStatus.HAVCO;
    }

    private Boolean HasSomethingBlockingTheWayAt(Position position) {
        return planet.HasAnObstacleAt(position);
    }
}

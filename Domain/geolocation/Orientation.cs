﻿public enum Orientation
{
    North,
    West,
    South,
    East
}

public static class OrientationMethods
{
    public static Orientation Left(this Orientation o)
    {
        switch (o)
        {
            case Orientation.North:
                return Orientation.West;
            case Orientation.West:
                return Orientation.South;
            case Orientation.South:
                return Orientation.East;
            case Orientation.East:
                return Orientation.North;
        }
        return o;
    }

    public static Orientation Right(this Orientation o)
    {
        switch (o)
        {
            case Orientation.North:
                return Orientation.East;
            case Orientation.East:
                return Orientation.South;
            case Orientation.South:
                return Orientation.West;
            case Orientation.West:
                return Orientation.North;
        }
        return o;
    }
}
﻿using Domain.geolocation;

public class Planet
{
    public Planet(Size size, List<Obstacle> obstacles, List<Location> pointsOfInterest)
    {
        Size = size;
        this.Obstacles = obstacles;
        this.pointsOfInterest = pointsOfInterest;
    }

    private List<Location> pointsOfInterest;

    public Size Size { get; init; }
    public List<Obstacle> Obstacles { get; init; } = Array.Empty<Obstacle>().ToList();
    public bool HasAnObstacleAt(Position position)
    {
        return Obstacles.Any(o => o.Position == position);
    }

    public Position RealPositionFrom(Position value)
    {
        return Size.Bound(value);
    }

    public Position LocationOf(String site)
    {
        return pointsOfInterest.Where(l => l.Name == site).First().Position;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.geolocation
{
    public record Location(String Name, Position Position);
}

﻿public record Position(int X, int Y) {

    private record Offset
    {
        public int Dx { get; }
        public int Dy { get; }

        public Offset(int dx, int dy)
        {
            Dx = dx;
            Dy = dy;
        }

    }

    private Position Project(Offset offset)
    {
        return new Position(X + offset.Dx, Y + offset.Dy);
    }

    public Position Project(Orientation orientation, Distance distance) {
        switch (orientation)
        {
            case { } o when o == Orientation.North:
                return Project(new Offset(0, distance.InMeters()));
            case { } o when o == Orientation.South:
                return Project(new Offset(0, -distance.InMeters()));
            case { } o when o == Orientation.East:
                return Project(new Offset(distance.InMeters(), 0));
            case { } o when o == Orientation.West:
                return Project(new Offset(-distance.InMeters(), 0));
        }
        return this;
    }
}


public static class PositionMethods {
    public static Distance meters(this int i)
    {
        return Distance.InMeters(i);
    }
} 
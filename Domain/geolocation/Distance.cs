﻿public record Distance
{
    public readonly int value;
    public int InMeters()
    {
        return value;
    }

    private Distance(int inMeters)
    {
        value = inMeters;
    }


    public static Distance InMeters(int inMeters)
    {
        return new Distance(inMeters);
    }
}

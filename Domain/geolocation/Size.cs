﻿public record Size
{
    public int Width { get; init; }
    public int Height { get; init; }

    internal Position Bound(Position value)
    {
        return new Position(bounded(value.X, Width), bounded(value.Y, Height));
    }

    private int bounded(int value, int limit)
    {
        if (value < 0)
        {
            return limit - (Math.Abs(value) % limit);
        }
        return value % limit;

    }
}
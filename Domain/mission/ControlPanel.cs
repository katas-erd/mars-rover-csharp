﻿namespace Domain.mission
{
    public class ControlPanel
    {
        Rover rover;
        private ControlPanel(Rover rover)
        {
            this.rover = rover;
        }

        public static ControlPanel Piloting(Rover rover) {
            return new ControlPanel(rover);
        }

        public Position CurrentPosition()
        {
            return rover.Position();
        }

        public Orientation CurrentOrientation()
        {
            return rover.Orientation();
        }

        public Rover Rover()
        {
            return rover;
        }

        public RoverStatus Send(String commands)
        {
            //commands.(c => apply(c));
            foreach (char command in commands.ToUpper())
            {
                var status = apply(command);
                if (status != RoverStatus.HAVCO)
                    return status;
            }
            return RoverStatus.HAVCO;
        }


        private RoverStatus apply(char command)
        {
            switch (command)
            {
                case 'F': return rover.moveForward();
                case 'B': return rover.moveBackward();
                case 'L': return rover.turnLeft();
                case 'R': return rover.turnRight();
                default:
                    {
                        Console.WriteLine($"Cannot perform {command}");
                        return RoverStatus.CANTCO;
                    }
            }
        }
    }

}

/*
class ControlPanel(piloting: Rover) {
    val rover = piloting

    fun send(commands: String): RoverStatus {
        commands
            .map { it.toCommand() }
            .forEach { doThat ->
                val status = rover.doThat()
                if (status != RoverStatus.HAVCO)
                    return@send status
            }
        return RoverStatus.HAVCO
    }

    private fun Char.toCommand(): Rover.() -> RoverStatus {
        return when (this.uppercase()) {
            "F" -> Rover::`move forward`
            "B" -> Rover::`move backward`
            "L" -> Rover::`turn left`
            "R" -> Rover::`turn right`
            else -> {
                { RoverStatus.CANTCO }
            }
        }
    }
}

*/
﻿namespace Domain.mission
{
    public record Report(String name, Position Position, Orientation Orientation, RoverStatus Status);


}

﻿

using System;

namespace Domain.mission
{

    public class Mission
    {
        private String name;
        private ControlPanel controlPanel;
        private MissionControlMonitor monitor;

        public Mission(string named, ControlPanel controledBy, MissionControlMonitor sendingReportTo)
        {
            this.name = named;
            this.controlPanel = controledBy;
            this.monitor = sendingReportTo;
        }

        public Report Transmit(String commands)
        {
            var status = controlPanel.Send(commands);
            var currentReport = report(status);
            monitor.display(currentReport);
            return currentReport;
        }

        private Report report(RoverStatus status)
        {
            return new Report(name, controlPanel.CurrentPosition(), controlPanel.CurrentOrientation(), status);
        }
    }


}

﻿public class Rover {
    private Orientation compas;
    private NavigationSystem navigationSystem;
    public Rover(NavigationSystem guidedWith, Orientation facing)
    {
        this.compas = facing;
        this.navigationSystem = guidedWith;
    }

    public Position Position()
    {
        return navigationSystem.Position();
    }

    public Orientation Orientation()
    {
        return this.compas;
    }

    public RoverStatus moveForward()
    {
        return navigationSystem.GoTo(compas, 1.meters());
    }

    public RoverStatus moveBackward()
    {
        return navigationSystem.GoTo(compas, (-1).meters());
    }

    public RoverStatus turnLeft()
    {
        compas = compas.Left();
        return RoverStatus.HAVCO;
    }

    public RoverStatus turnRight()
    {
        compas = compas.Right();
        return RoverStatus.HAVCO;
    }
}
